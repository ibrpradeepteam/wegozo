<?php
// BuddyPress hacks and mods will go here
// Read More: https://codex.buddypress.org/themes/bp-custom-php/

// Remove links from Profile fields
/*function remove_xprofile_links() {
    remove_filter( 'bp_get_the_profile_field_value', 'xprofile_filter_link_profile_data', 9, 2 );
}
add_action( 'bp_init', 'remove_xprofile_links' );*/

/**
 * Change BuddyPress default Members landing tab.
 */
define('BP_DEFAULT_COMPONENT', 'profile' );

function remove_xprofile_links() {
    remove_filter( 'bp_get_the_profile_field_value', 'xprofile_filter_link_profile_data', 9, 2 );
}
add_action('bp_setup_globals', 'remove_xprofile_links');

/* Re-order profile section menu */
function my_change_profile_tab_order() {
	global $bp;
	$bp->bp_nav['profile']['position'] = 10;
	$bp->bp_nav['activity']['position'] = 20;		
	$bp->bp_nav['friends']['position'] = 30;
	/*$bp->bp_nav['groups']['position'] = 40;
	$bp->bp_nav['blogs']['position'] = 50;
	$bp->bp_nav['messages']['position'] = 60;
	$bp->bp_nav['settings']['position'] = 70;
	$bp->bp_nav['history']['position'] = 80;*/		
}
add_action( 'bp_setup_nav', 'my_change_profile_tab_order', 999 );

add_action('bp_init','change_media_tab_position', 12);  
function change_media_tab_position(){  
    global $bp;  
    if( isset ($bp->bp_nav['media'])){  
        $bp->bp_nav['media']['position'] = 25;  
    }  
}

?>