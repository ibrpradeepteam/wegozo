<?php
// BuddyPress hacks and mods will go here
// Read More: https://codex.buddypress.org/themes/bp-custom-php/

// Remove links from Profile fields
/*function remove_xprofile_links() {
    remove_filter( 'bp_get_the_profile_field_value', 'xprofile_filter_link_profile_data', 9, 2 );
}
add_action( 'bp_init', 'remove_xprofile_links' );*/


function remove_xprofile_links() {
    remove_filter( 'bp_get_the_profile_field_value', 'xprofile_filter_link_profile_data', 9, 2 );
}
add_action('bp_setup_globals', 'remove_xprofile_links');

?>