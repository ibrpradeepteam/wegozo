<?php

/*
Plugin Name: Wegozo Profile Image in Sidebar
Plugin URI: http://barcelonaonlinemarketing.com
Description: Plugin to add sidebar widget with profile image on Buddypress profile pages.
Author: Barcelonaonlinemarketing.com
Version: 1.1
Author URI: http://barcelonaonlinemarketing.com
*/

class wegozo_sidebar_profile_data extends WP_Widget {

	// constructor
	function wegozo_sidebar_profile_data() {
	        parent::WP_Widget(false, $name = __('Wegozo BuddyPress Profile Image', 'wp_widget_plugin') );
    
	}

// widget form creation
function form($instance) {

// Check values
if( $instance) {
     $title = esc_attr($instance['title']);
     $textarea = $instance['textarea'];
} else {
     $title = '';
     $textarea = '';
}
?>

<p>
<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'wp_widget_plugin'); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
</p>


<p>
<label for="<?php echo $this->get_field_id('textarea'); ?>"><?php _e('Description:', 'wp_widget_plugin'); ?></label>
<textarea class="widefat" id="<?php echo $this->get_field_id('textarea'); ?>" name="<?php echo $this->get_field_name('textarea'); ?>" rows="7" cols="20" ><?php echo $textarea; ?></textarea>
</p>
<?php
}

	
function update($new_instance, $old_instance) {
      $instance = $old_instance;
      // Fields
      $instance['title'] = strip_tags($new_instance['title']);
      $instance['textarea'] = strip_tags($new_instance['textarea']);
     return $instance;
}
	

   // display widget
function widget($args, $instance) {
   extract( $args );
   
   // these are the widget options
   $title = apply_filters('widget_title', $instance['title']);
   $textarea = $instance['textarea'];
   echo $before_widget;
  
  // Display the widget
   echo '<div class="widget-text wp_widget_plugin_box">';
   // ORIGINAL STYLING echo '<div class="widget-text wp_widget_plugin_box" style="width:269px; padding:5px 9px 20px 5px; border: 1px solid rgb(231, 15, 52); background: #1fb3dd; border-radius: 5px; margin: 10px 0 25px 0;">';
   // ORIGINAL STYLING echo '<div class="widget-title" style="width: 90%; height:30px; margin-left:3%; ">';
   echo '<div class="widget-title">';   
   
   // Check if title is set
   if ( $title ) {
      echo  $before_title . $title . $after_title ;
   }
   echo '</div>';
  
  // Check if textarea is set
  // ORIGINAL echo '<div class="widget-textarea" style="width: 90%; margin-left:3%; padding:8px; background-color: white; border-radius: 3px; min-height: 70px;">';
   echo '<div class="widget-textarea">';
   
   // Display User Profile Image
   //if (bp_is_user_profile()) : 
   
   // Display if on any of the BP user pages, not just profile page
   if (bp_is_user()) :
   
	   echo '<div id="item-header-avatar" class="rounded">';
	   //echo '<a href="'.bp_displayed_user_link();'">';
	   echo '<a href="';
	   bp_displayed_user_link();
	   echo '">';
	   
	   bp_displayed_user_avatar( 'type=full' );
	   echo '</a>';
	
	   //echo '</a>';
	   do_action('bp_member_online_status', bp_displayed_user_id());
	   echo '</div><!-- #item-header-avatar -->';
   
   endif;
   // END Display User Profile Image
   
   //if( $textarea ) {
     //echo '<p class="wp_widget_plugin_textarea" style="font-size:15px;">'.$textarea.'</p>';
   //}
   echo '</div>';
   echo '</div>';
   echo $after_widget;
}
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("wegozo_sidebar_profile_data");')); 
?>