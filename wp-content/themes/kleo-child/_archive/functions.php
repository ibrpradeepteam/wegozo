<?php
/**
 * @package WordPress
 * @subpackage Kleo
 * @author SeventhQueen <themesupport@seventhqueen.com>
 * @since Kleo 1.0
 */

/**
 * Kleo Child Theme Functions
 * Add custom code below
*/ 


/* Remove forum counts on forum index pages - e.g. France (0,0) */
function my_remove_list_forums_counts( $args = array() ) {
	if ( ! empty( $args ) ) {
		$args['show_topic_count'] = false;
		$args['show_reply_count'] = false;	
	}
	return $args;
}
add_filter( 'bbp_after_list_forums_parse_args', 'my_remove_list_forums_counts' );


