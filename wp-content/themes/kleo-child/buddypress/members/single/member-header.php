<?php

/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<?php do_action( 'bp_before_member_header' ); ?>


<?php
// Check for Maps on User Profile Page Only
if (bp_is_user_profile()) :

	// Assign Server Domain - Staging or Live
	$wegozo_server = "wegozo.com"; // Default to live server
	
	if (isset($_SERVER['HTTP_HOST'])) :	// Check server variables are working		
		$wegozo_server = $_SERVER['HTTP_HOST']; // if set, assign domain to variable
	endif;

	// Get User ID of this profile
	$wegozo_profile_id = bp_displayed_user_id();
	global $wpdb, $bp;	
	$Sql = "SELECT map_id FROM user_map_mapping WHERE user_id = '".$wegozo_profile_id."'";
	$map_id = $wpdb->get_results($Sql);	
	$wegozomap_id = (empty($map_id[0]->map_id))? 5 : $map_id[0]->map_id;
	//$wegozomap_id = $map_id[0]->map_id;
	//$wegozomap_id = $map_id[0]->map_id;
	
			
	// Assign Map to Profile of this user	
	/*switch ($wegozo_profile_id) {
		case 0:
			break;
		case 1: // USERID Tom
			$wegozomap_id = 1; // Map ID
			break;
		case 2: // USERID Jay
			$wegozomap_id = 3; // Map ID
			break;
		case 105: // USERID Julia G Lopez etc.
			$wegozomap_id = 4;
			break;
		case 104:
			$wegozomap_id = 6;
			break;
		case 122:
			$wegozomap_id = 7;
			break;
		case 102:
			$wegozomap_id = 8;
			break;
		case 111:
			$wegozomap_id = 9;
			break;	
		case 124:
			$wegozomap_id = 10;
			break;	
		case 131:
			$wegozomap_id = 11;
			break;				
		case 133:
			$wegozomap_id = 12;
			break;
		case 135:
			$wegozomap_id = 13;
			break;
		case 116:
			$wegozomap_id = 14;
			break;
		case 130:
			$wegozomap_id = 15;
			break;
		case 103:
			$wegozomap_id = 16;
			break;
		case 142:
			$wegozomap_id = 17;
			break;
		case 145:
			$wegozomap_id = 18;
			break;
		case 118:
			$wegozomap_id = 19;
			break;
		case 129:
			$wegozomap_id = 20;
			break;
		case 152:
			$wegozomap_id = 21;
			break;
		case 161:
			$wegozomap_id = 22;
			break;
		case 106:
			$wegozomap_id = 23;
			break;
		case 126:
			$wegozomap_id = 24;
			break;
		case 115:
			$wegozomap_id = 25;
			break;
		case 133:
			$wegozomap_id = 12;
			break;
		case 166:
			$wegozomap_id = 27;
			break;
		case 164:
			$wegozomap_id = 26;
			break;
		case 165:
			$wegozomap_id = 28;
			break;
		case 167:
			$wegozomap_id = 29;
			break;
		case 177:
			$wegozomap_id = 30;
			break;
		case 180:
			$wegozomap_id = 31;
			break;
		case 157:
			$wegozomap_id = 32;
			break;
		case 158:
			$wegozomap_id = 33;
			break;
		case 169:
			$wegozomap_id = 34;
			break;
		case 184:
			$wegozomap_id = 35;
			break;
		case 143:
			$wegozomap_id = 36;
			break;	
		case 133:
			$wegozomap_id = 12;
			break;	
		case 194:
			$wegozomap_id = 37;
			break;
		case 212:
			$wegozomap_id = 38;
			break;			
		case 102:
			$wegozomap_id = 8;
			break;
		case 144:
			$wegozomap_id = 39;
			break;
		case 123:
			$wegozomap_id = 40;
			break;
		case 243:
			$wegozomap_id = 41;
			break;
		case 37:
			$wegozomap_id = 42;
			break;
		case 136:
			$wegozomap_id = 43;
			break;
		case 155:
			$wegozomap_id = 44;
			break;
		case 107:
			$wegozomap_id = 45;
			break;	
		case 287:
			$wegozomap_id = 46;
			break;																																																																																												
		default: // Display blank map
			$wegozomap_id = 5;			
	}	*/
	 

	// Four Scenarios //
	// Viewing own profile page and HAS populated their map - Show Map and Show Update Links
	// Viewing own profile page has NOT populated map - Show Default Map and Show Update Links
	// On someone else's profile page who HAS populated map - Show Map and DON'T SHOW update links 
	// On someone else's page who has NOT populated map - Don't show map, Don't show update links


	// ---------------------------------------
	// User is viewing their own profile page
	// ---------------------------------------
		
	if (bp_is_my_profile()) : // If the current user is viewing their OWN profile, display map, give them an option to edit the map and bucket list
		echo '<h3>My Travel Map</h3>';
	?>
		<p style="margin-bottom:20px;">
        	To update your travel map, please 
            <a href="http://<?php echo($wegozo_server); ?>/members/<?php bp_displayed_user_username(); ?>/profile/edit/group/4/#item-body">click here</a>. 
            To update your Bucket List 
            <a href="http://<?php echo($wegozo_server); ?>/members/<?php bp_displayed_user_username(); ?>/profile/edit/group/3/#item-body">click here</a>.</p>
   
   <?php
   
   	
		echo '<div style="width:90%;">';
		//echo 'Map ID = ' . $wegozomap_id;		
		build_i_world_map($wegozomap_id);		
		echo '</div>';

	// ---------------------------------------
	// User is viewing someone else's profile page
	// ---------------------------------------	
	else:
		// Check if the map is NOT the default map 5 so it has been populated. Display it if so.
		if ($wegozomap_id!= 5) :
			echo '<h3>My Travel Map</h3>';
			echo '<div style="width:90%;">';		
			build_i_world_map($wegozomap_id);		
			echo '</div>';		
		endif;	
	
	endif;

endif;
 ?>


<div id="item-header-avatar" class="rounded">
	<a href="<?php bp_displayed_user_link(); ?>">

		<?php bp_displayed_user_avatar( 'type=full' ); ?>

	</a>
  <?php do_action('bp_member_online_status', bp_displayed_user_id()); ?>
</div><!-- #item-header-avatar -->

<div id="item-header-content" <?php if (isset($_COOKIE['bp-profile-header']) && $_COOKIE['bp-profile-header'] == 'small') {echo 'style="display:none;"';} ?>>

	<?php if ( bp_is_active( 'activity' ) && bp_activity_do_mentions() ) : ?>
		<h4 class="user-nicename">@<?php bp_displayed_user_mentionname(); ?></h4>
	<?php endif; ?>

	<span class="activity"><?php bp_last_activity( bp_displayed_user_id() ); ?></span>

	<?php do_action( 'bp_before_member_header_meta' ); ?>

	<div id="item-meta">

		<?php if ( bp_is_active( 'activity' ) ) : ?>

			<div id="latest-update">

				<?php bp_activity_latest_update( bp_displayed_user_id() ); ?>

			</div>

		<?php endif; ?>

		<div id="item-buttons">

			<?php do_action( 'bp_member_header_actions' ); ?>

		</div><!-- #item-buttons -->

		<?php 
		/***
		 * If you'd like to show specific profile fields here use:
		 * bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
		 */
		 do_action( 'bp_profile_header_meta' );

		 ?>

	</div><!-- #item-meta -->

</div><!-- #item-header-content -->

<?php do_action( 'bp_after_member_header' ); ?>

<?php do_action( 'template_notices' ); ?>