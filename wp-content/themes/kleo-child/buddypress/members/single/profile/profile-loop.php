<?php do_action( 'bp_before_profile_loop_content' ); ?>

<?php if ( bp_has_profile() ) : ?>

	<?php while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

		<?php if ( bp_profile_group_has_fields() ) : ?>

			<?php do_action( 'bp_before_profile_field_content' ); ?>

			<div class="bp-widget <?php bp_the_profile_group_slug(); ?>">
                		
				<div class="hr-title hr-full hr-double"><abbr><?php bp_the_profile_group_name(); ?></abbr></div>
				<div class="gap-10"></div>
                <?php 
				// Get the group ID of this group
				// Also see bp_get_the_profile_group_name()
				
				$wegozo_groupid = bp_get_the_profile_group_id();
				//echo('Profile group id: '.$wegozo_groupid);
				
				// If it's the group ID of "My Bucket list" group		
				if ($wegozo_groupid == 3): ?>
                    <div class="row"><div class="col-sm-12"><img src="http://wegozo.com/wp-content/uploads/2015/01/wegozo_bucket_list_image_852x148_2.png" alt="Wegozo Bucket List" width="100%" class="aligncenter size-full wp-image-2600" /></div></div>
                    <div class="gap-10"></div>  
                    <div class="row"><div class="col-sm-12"><p>These are some of the things I would like to do before I <em>kick the bucket</em>...</p></div></div>                  
                <?php endif; // End display My Bucket List image ?>
                
					<?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>

						<?php if ( bp_field_has_data() ) : ?>
              
              
              
              <dl<?php bp_field_css_class('dl-horizontal'); ?>>
                <dt><?php bp_the_profile_field_name(); ?></dt>
                <dd><?php bp_the_profile_field_value(); ?></dd>
              </dl>
              

						<?php endif; ?>

						<?php do_action( 'bp_profile_field_item' ); ?>

					<?php endwhile; ?>

                        
                        
                        
			</div><!-- end bp-widget -->

			<?php do_action( 'bp_after_profile_field_content' ); ?>

		<?php endif; ?>

	<?php endwhile; ?>

	<?php do_action( 'bp_profile_field_buttons' ); ?>

<?php endif; ?>

<?php do_action( 'bp_after_profile_loop_content' ); ?>
