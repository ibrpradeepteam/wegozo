<?php
/**
 * @package WordPress
 * @subpackage Kleo
 * @author SeventhQueen <themesupport@seventhqueen.com>
 * @since Kleo 1.0
 */

/**
 * Kleo Child Theme Functions
 * Add custom code below
*/ 


/* Remove forum counts on forum index pages - e.g. France (0,0) */
function my_remove_list_forums_counts( $args = array() ) {
	if ( ! empty( $args ) ) {
		$args['show_topic_count'] = false;
		$args['show_reply_count'] = false;	
	}
	return $args;
}
add_filter( 'bbp_after_list_forums_parse_args', 'my_remove_list_forums_counts' );

/* Change default titles on category archive pages etc */
/*function kleo_title()
{
	$output = "";

	if ( is_category() )
	{
		$output = __('','kleo_framework')." ".single_cat_title('',false);
	}

	return $output;
}*/

//Add me to child theme functions.php
function kleo_title()
{
	$output = "";

	if ( is_category() )
	{
		$output = __('','kleo_framework')." ".single_cat_title('',false);
	}
	elseif (is_day())
	{
		$output = __('Archive for date:','kleo_framework')." ".get_the_time('F jS, Y');
	}
	elseif (is_month())
	{
		$output = __('Archive for month:','kleo_framework')." ".get_the_time('F, Y');
	}
	elseif (is_year())
	{
		$output = __('Archive for year:','kleo_framework')." ".get_the_time('Y');
	}
	elseif (is_search())
	{
		global $wp_query;
		if(!empty($wp_query->found_posts))
		{
			if($wp_query->found_posts > 1)
			{
				$output =  $wp_query->found_posts ." ". __('search results for:','kleo_framework')." ".esc_attr( get_search_query() );
			}
			else
			{
				$output =  $wp_query->found_posts ." ". __('search result for:','kleo_framework')." ".esc_attr( get_search_query() );
			}
		}
		else
		{
			if(!empty($_GET['s']))
			{
				$output = __('Search results for:','kleo_framework')." ".esc_attr( get_search_query() );
			}
			else
			{
				$output = __('To search the site please enter a valid term','kleo_framework');
			}
		}

	}
	elseif (is_author())
	{
		$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
		$output = __('Author Archive','kleo_framework')." ";

		if(isset($curauth->nickname)) $output .= __('for:','kleo_framework')." ".$curauth->nickname;

	}
	elseif (is_tag())
	{
		$output = __('Tag Archive for:','kleo_framework')." ".single_tag_title('',false);
	}
	elseif(is_tax())
	{
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$output = __('Archive for:','kleo_framework')." ".$term->name;

	} elseif ( is_front_page() && !is_home() ) {
					$output = get_the_title(get_option('page_on_front'));

	} elseif ( is_home() && !is_front_page() ) {
					$output = get_the_title(get_option('page_for_posts'));

	} elseif ( is_404() ) {
					$output = __('Error 404 - Page not found','kleo_framework');
	}
	else {
		$output = get_the_title();
	}

	if (isset($_GET['paged']) && !empty($_GET['paged']))
	{
		$output .= " (".__('Page','kleo_framework')." ".$_GET['paged'].")";
	}

	return $output;
}


/* Allow HTML to be entered in Category Description fields */
remove_filter('pre_term_description', 'wp_filter_kses');



/* Change default image size for posts - See Abe's reply here: */ 
/* http://seventhqueen.com/support/forums/topic/where-are-uploaded-image-sized-defined */

add_action( 'after_setup_theme', 'kleo_setup_image_size', 11 );
function kleo_setup_image_size() {
//set_post_thumbnail_size( 672, 900 );
add_image_size( 'kleo-full-width', 894, 1400 ); /* 894 is the max width a landscape image will display on a blog posts and pages, 1400 is the max height a portrait image will display in blog post with sidebar.  */
}


/* 
Change KLEO default image size for Masonry grids etc. See DropBox > Wegozo > Documentation > IMages sizes for website.
This is because with a 3 column Mansonry view, KLEO displays images at:

Width: 276 or Width: 284 depending on which screen resolution is viewing them. 480 width is therefore overkill.

This has been changed because by default KLEO was creating images that are:
Landcape : 480px x 345px
Portrait : 480px x 640px

Meaning a 69% larger image was being loaded than necessary.
*/

add_action( 'after_setup_theme', 'kleo_change_image_sizes' );
function kleo_change_image_sizes() {
	global $kleo_config;
 
	//Post image sizes for carousels and galleries
	$kleo_config['post_gallery_img_width'] = 400;
	$kleo_config['post_gallery_img_height'] = 225;
}

 function InsertMapRecord($POST){	
			global $wpdb, $bp;	
			//field_26 is post array key value which containt countries name
			if(isset($POST['field_26'])){ 	
				foreach($POST['field_26'] as $cont){	
					$countries .=$cont.','.$cont.',,,#1fb3dd;';	
				}
			}
			
			$user = wp_get_current_user();
			$user = (array)$user->data;
			
			$Sql = "SELECT map_id FROM user_map_mapping WHERE user_id = '".$user['ID']."'";	
			$map_id = $wpdb->get_results($Sql);	
			$map_id = $map_id[0]->map_id;
			
	        if(empty($map_id)){
				$type = "insert";
			}else{
				$type = "update";
			}
			
			if($type == 'insert'){
					$wpdb->insert('wp_a0mjknwzpx_i_world_map',
							 array(
							 'name' 			=>$user['display_name'].'(User ID '.$user['ID'].')', 	
							 'use_defaults'		=> 1, 	
							 'bg_color' 		=> '#FFFFFF', 	
							 'border_color' 	=> '#CCCCCC', 	
							 'border_stroke' 	=> 0, 	
							 'ina_color'		=> '#C7D1C8', 	
							 'act_color'		=> '#1fb3dd', 	
							 'marker_size'		=> 10, 	
							 'aspect_ratio' 	=> 1,	
							 'interactive'  	=> 1,	
							 'showtooltip'  	=> 1,	
							 'region'			=> 'world,countries', 	
							 'display_mode'		=> 'regions', 	
							 'map_action' 		=> 'none', 	
							 'places'			=> $countries	
							 )	
							 );							 
	
			   $lastid = $wpdb->insert_id;
	
			   $wpdb->insert('user_map_mapping',	
							 array(	
							 'user_id'		=> $user['ID'], 	
							 'map_id' 		=> $lastid)	
							 );							 
	
			}elseif ($type == 'update'){										
					//wp_die(‘pradeep1’);
				$wpdb->update('wp_a0mjknwzpx_i_world_map',	
								 array(	
								 'name' 			=>$user['display_name'].'(User ID '.$user['ID'].')', 	
								 'use_defaults'		=> 1, 	
								 'bg_color' 		=> '#FFFFFF', 	
								 'border_color' 	=> '#CCCCCC', 	
								 'border_stroke' 	=> 0, 	
								 'ina_color'		=> '#C7D1C8', 	
								 'act_color'		=> '#1fb3dd', 	
								 'marker_size'		=> 10, 	
								 'aspect_ratio' 	=> 1,	
								 'interactive'  	=> 1,	
								 'showtooltip'  	=> 1,	
								 'region'			=> 'world,countries', 	
								 'display_mode'		=> 'regions', 	
								 'map_action' 		=> 'none', 	
								 'places'			=> $countries	
								 ),	
								 array('id' => $map_id)	
							 );	
			}	
}

function insertmap(){
		if(isset($_POST['field_26'])){
			InsertMapRecord($_POST);
		}		
}

add_action( 'xprofile_data_after_save', 'insertmap' );